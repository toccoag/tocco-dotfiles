# Tocco Dotfiles

## Content

* `root/`

  Files shipped via Debian package.

* `bin/`

  Contains some useful scripts to make your lifes easier. Add `export PATH="$HOME/PATH/TO/THIS/REPO/bin:$PATH"` to your ~/.profile
  and restart your terminal to make the commands available on your machine.

  See [bin](bin) directory for more details.

* `ssh/` - configuration for OpenSSH

  Symlinks providing long-term static paths to access our SSH config
  and known_hosts files. For use by those not able to use our Debian
  package.

## Changes to the package

When making changes to the package, be sure to update the version
in `root/DEBIAN/control`. Other changes, like pipeline changes,
which don't affect the package content need no version increment.
